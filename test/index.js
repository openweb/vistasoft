import fs from 'fs';
import pako from 'pako';
import Papa from 'papaparse';

import imoveis_url from '../data/indice.tar.gz';
import caracteristicas_url from '../data/imovel_caracteristicas.tar.gz';

const loadImoveis = () => fetch(imoveis_url).then(resp => {
    if (!resp.ok) {
        throw resp;
    }
    return resp.arrayBuffer();
}).then(text => {
    return pako.inflate(text, {to: 'string'});    
}).then(Papa.parse)
.then(({ data }) => data)
.then(rows => rows.map(([id, tipo_id, tipo, lat, lng, uf, cidade, bairro, valor, dormitorios, vagas ]) => 
    ({
        id,
        tipo_id: +tipo_id,
        tipo,
        lat: +lat,
        lng: +lng,
        uf,
        cidade,
        bairro,
        valor: +valor,
        dormitorios: +dormitorios,
        vagas: +vagas
    })
)).then(imoveis => imoveis.reduce((acc, imovel) => {
    acc.imoveis.push(imovel);
    
    let cidade = acc.cidades.find(c => c.titulo == imovel.cidade);
    if (!cidade) {
        cidade = {
            uf: imovel.uf,
            titulo: imovel.cidade,
            bairros: []
        }
        acc.cidades.push(cidade);
    }
    let bairro = cidade.bairros.find(b => b.titulo == imovel.bairro);
    if (!bairro) {
        bairro = {
            titulo: imovel.bairro
        };
        cidade.bairros.push(bairro);
    }

    let tipo = acc.tipos.find(t => t.id == imovel.tipo_id);
    if (!tipo) {
        tipo = {
            id: imovel.tipo_id,
            titulo: imovel.tipo
        };
        acc.tipos.push(tipo);
    }

    return acc;
}, {imoveis:[], tipos: [], cidades: []}));

const loadCaracteristicas = () => fetch(caracteristicas_url).then(resp => {
    if (!resp.ok) {
        throw resp;
    }
    return resp.arrayBuffer();
}).then(text => {
    return pako.inflate(text, { to: 'string' });
}).then(Papa.parse)
    .then(({ data }) => data)
    .then(rows => rows.map(([imovel_id, titulo, condominio]) =>
        ({
            imovel_id,
            titulo,
            condominio
        })
    )).then(caracteristicas => caracteristicas.reduce((acc, { imovel_id, titulo, condominio}) => {
        

        let caracteristica = acc.caracteristicas.find(c => c.titulo == titulo);
        if (!caracteristica) {
            caracteristica = {
                titulo
            };
            acc.caracteristicas.push(caracteristica);
        }

        if (!acc.imovel_caracteristica[imovel_id]) {
            acc.imovel_caracteristica[imovel_id] = [];
        }

        acc.imovel_caracteristica[imovel_id].push({
            imovel_id,
            titulo,
            condominio: !!condominio,
            imovel: !condominio
        });

        return acc;
    }, { caracteristicas: [], imovel_caracteristica: {} }));
    


Promise.all([loadImoveis(), loadCaracteristicas()])
    .then(([{ imoveis, tipos, cidades }, {caracteristicas, imovel_caracteristica }]) => {
        
        imoveis.forEach(imovel => {
            imovel.caracteristicas = Object.values(imovel_caracteristica[imovel.id]||{});
        });
        
        return {
            imoveis,
            tipos,
            cidades,
            caracteristicas
        }
    }).then(console.log)