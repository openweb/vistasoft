<?php

namespace VistaSoft;


use VistaSoft\Utils\Str;

class Imoveis
{
    private static $indice = null;
    private static $imoveis_folder = null;
    private static $imoveis = null;
    public static $codigos = null;

    public static function reinit() {
        static::$indice = null;
        static::$imoveis = null;
        static::$codigos = null;
    }

    public static function reloadcache() {
        Api::exec('reloadcache');
    }

    public static function listarCampos()
    {
        return Api::exec("imoveis/listarcampos");
    }

    public static function listar($fields = array('Codigo'), $filter = null, $order = null, $paginacao = array("pagina" => 1, "quantidade" => 50))
    {
        $pesquisa = array(
            "fields" => $fields,
            "filter" => $filter,
            "order" => $order,
            "paginacao" => $paginacao
        );

        $resp = Api::exec("imoveis/listar", array(
            "pesquisa" => json_encode($pesquisa),
            "showtotal" => 1,
            "finalidade" => "venda"
        ));



        $total = (int) $resp->total;
        $paginas = (int) $resp->paginas;
        $pagina = (int) $resp->pagina;
        $quantidade = (int) $resp->quantidade;
        $paginacao = (object) array(
            "total" => $total,
            "paginas" => $paginas,
            "pagina" => $pagina,
            "quantidade" => $quantidade
        );
        $imoveis = array();
        foreach ((array) $resp as $codigo => $imovel) {
            if (in_array($codigo, array("total", "paginas", "pagina", "quantidade"))) {
                continue;
            }
            $imoveis[$codigo] = $imovel;
        }
        return (object) array(
            "imoveis" => $imoveis,
            "paginacao" => $paginacao
        );
    }

    public static function detalhes($codigo, $fields = array('Codigo'))
    {
        $pesquisa = array(
            "fields" => $fields
        );

        $resp = Api::exec("imoveis/detalhes", array(
            "pesquisa" => json_encode($pesquisa),
            "imovel" => $codigo
        ));

        return $resp;
    }

    public static function detalhesAsync($codigo, $fields = array('Codigo'))
    {
        $pesquisa = array(
            "fields" => $fields
        );

        $promise = Api::execAsync("imoveis/detalhes", array(
            "pesquisa" => json_encode($pesquisa),
            "imovel" => $codigo
        ));

        return $promise;
    }

    private static function loadIndice()
    {
        $data_folder = Api::getDataFolder();
        $filename = $data_folder . '/indice.php';
        if (!file_exists($filename)) {
            return array();
        }

        $indice = require $filename;
        return (array) $indice;
    }

    private static function saveIndice($indice)
    {
        $data_folder = Api::getDataFolder();
        $filename = $data_folder . '/indice.php';

        $content = "<?php return " . var_export($indice, true) . ";";
        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function getIndice()
    {
        if (null === static::$indice) {
            static::$indice = static::loadIndice();
        }
        return static::$indice;
    }

    public static function setIndice($indice)
    {
        static::$indice = $indice;
        static::saveIndice($indice);
    }

    public static function getImoveisFolder()
    {
        if (null === static::$imoveis_folder) {
            $data_folder = Api::getDataFolder();
            $folder = $data_folder . '/imoveis';
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }
            static::$imoveis_folder = $folder;
        }
        return static::$imoveis_folder;
    }

    public static function saveImovel($imovel)
    {
        $data_folder = static::getImoveisFolder();
        $filename = $data_folder . '/' . $imovel->Codigo . '.php';
        $content = '<?php return (object)' . var_export((array) $imovel, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function loadImovel($codigo)
    {
        $data_folder = static::getImoveisFolder();
        $filename = $data_folder . '/' . $codigo . '.php';
        if (!file_exists($filename)) {
            return null;
        }
        return require $filename;
    }

    public static function removeImovel($codigo)
    {
        $data_folder = static::getImoveisFolder();
        $filename = $data_folder . '/' . $codigo . '.php';
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    public static function update()
    {
        $pagina = 1;
        $indice = static::getIndice();
        $remover = $indice;
        $atualizados = 0;
        $removidos = 0;

        static::reloadcache();

        do {
            $atualizar = array();
            $start = microtime(true);
            $resp = static::listar(array('Codigo', 'DataHoraAtualizacao'), null, null, array("pagina" => $pagina, "quantidade" => 50));
            $paginacao = $resp->paginacao;
            $paginas = $paginacao->paginas;


            $imoveis = $resp->imoveis;

            foreach ($imoveis as $codigo => $imovel) {
                if (!isset($indice[$codigo])) {
                    $indice[$codigo] = $imovel;
                    $atualizar[$codigo] = $imovel;
                } else {
                    unset($remover[$codigo]);

                    $oldImovel = $indice[$codigo];
                    $oldTime = strtotime($oldImovel->DataHoraAtualizacao);
                    $newTime = strtotime($imovel->DataHoraAtualizacao);

                    if ($newTime > $oldTime) {
                        $atualizar[$codigo] = $imovel;
                    }
                }
            }


            $promises = array();
            foreach ($atualizar as $codigo => $imovel) {
                //antigos
                $antigos = array('AceitaDacao','AceitaFinanciamento','AdministradoraCondominio',"Salas",'AnoConstrucao','AptosAndar','AptosEdificio','AreaPrivativa','AreaTotal','Bairro','BairroComercial','BanheiroSocialQtd','Bloco','Categoria','CEP','Chave','Cidade','Complemento','Construtora','DataAtualizacao','DataCadastro','DescricaoWeb','DimensoesTerreno','Dormitorios','Empreendimento','Endereco','Exclusivo','ExibirNoSite','Finalidade','GaragemNumeroBox','GaragemTipo','Imediacoes','Lancamento','Matricula','Numero','Ocupacao','Orientacao','Pais','Prestacao','Referencia','SaldoDivida','Situacao','Status','Suites','SuperDestaqueWeb','TemPlaca','TipoEndereco','TituloSite','UF','URLVideo','Vagas','ValorCondominio','ValorIptu','ValorLocacao','ValorVenda','VivaRealDestaqueVivaReal','Zona','CategoriaImovel','CategoriaMestre','CategoriaGrupo','ImoCodigo','Moeda','ConverterMoeda','MoedaIndice','CodigoEmpresa','CodigoEmp','CodigoEmpreendimento','CodigoCategoria','CodigoMoeda','CodigoProprietario','Proprietario','FotoProprietario','DataHoraAtualizacao','VideoDestaque','VideoDestaqueTipo','FotoDestaque','FotoDestaquePequena','FotoDestaqueEmpreendimento','FotoDestaqueEmpreendimentoPequena','Latitude','Longitude','Agenciador','CodigoCorretor','CodigoAgencia','TotalBanheiros', "EmpreendimentoSite");
                
                //novos
                $novos = array("AceitaDacao","AceitaFinanciamento","AceitaPermuta","Salas","AceitaPermutaCarro","AceitaPermutaOutro","AceitaPermutaTipoVeiculo","AdministradoraCondominio","AlugarJaDestaque","AnoConstrucao","AnoMinimoVeicPermuta","AptosAndar","AptosEdificio","AreaBoxPrivativa","AreaBoxTotal","AreaConstruida","AreaPrivativa","AreaTerreno","AreaTotal","Bairro","BairroComercial","BanheiroSocialQtd","Bloco","CampanhaImportacao","Categoria","CEP","Chave","ChaveNaAgencia","ChavesNaMaoDestaque","Cidade","ClasseDoImovel","CodigoMalote","Complemento","Construtora","CorretorPrimeiroAge","DataAtualizacao","DataCadastro","DataDisponibilizacao","DataEntrega","DataFimAutorizacao","DataImportacao","DataInicioAutorizacao","DataLancamento","DescricaoEmpreendimento","DescricaoWeb","DestaqueWeb","DimensoesTerreno","Dormitorios","EEmpreendimento","EmitiuNotaFiscalComissao","EmpOrulo","Empreendimento","Endereco","Exclusivo","ExclusivoCorretor","ExibirComentarios","ExibirNoSite","Finalidade","FolhaSPModelo","GaragemNumeroBox","GaragemTipo","GrupoSPTipoOferta","HoraDomFim","HoraDomInicio","HoraFerFim","HoraFerInicio","HoraSabFim","HoraSabInicio","HoraSegSexFim","HoraSegSexInicio","Imediacoes","ImovelaVendaDestaqueImovelaVenda","ImovelwebModelo","ImportadoMalote","Incompleto","Incorporadora","InformacaoVenda","InicioObra","InscricaoMunicipal","KeywordsWeb","Lancamento","Limpeza","LocacaoAnual","LocacaoTemporada","LocalizacaoPermuta","LojasEdificio","Lote","LugarCertoDestaqueLugarCerto","Matricula","MercadoLivreTipoML","MesConstrucao","Midia","Modulos","Monitoramento","Numero","Observacoes","ObsLocacao","ObsVenda","Ocupacao","OLXFinalidadesPublicadas","Orientacao","Orulo","PadraoConstrucao","Pais","Pavimentos","PercentualComissao","Plantao","PlantaoNoLocal","PorteEstrutural","Posicao","PosicaoAndar","PotenciaKVA","PrazoDesocupacao","Prestacao","ProjetoArquitetonico","Proposta","PropostaLocacao","QntDormitoriosPermuta","QntGaragensPermuta","QntSuitesPermuta","QTDGalpoes","QtdVarandas","Quadra","Reajuste","Referencia","Regiao","ResponsavelReserva","SaldoDivida","Setor","Situacao","Status","Suites","SummerSale","SuperDestaqueWeb","TemPlaca","TextoAnuncio","TipoEndereco","TipoImovel","TipoImovelPermuta","TipoOferta321Achei","TipoTeto","TiqueImoveisEmDestaque","TituloSite","TotalComissao","UF","URLVideo","Vagas","VagasCobertas","VagasDescobertas","ValorACombinar","ValorComissao","ValorCondominio","ValorCondominioM2","ValorDiaria","ValorIptu","ValorIPTUM2","ValorLimpeza","ValorLivreProprietario","ValorLocacao","ValorLocacaoM2","ValorM2","ValorPermutaImovel","ValorTotalAluguel","ValorVenda","ValorVendaM2","Venda","VigiaExterno","VigiaInterno","Visita","VisitaAcompanhada","VivaRealDestaqueVivaReal","WebEscritoriosDestaque","ZapTipoOferta","ZeladorNome","ZeladorTelefone","Zona","CategoriaImovel","CategoriaMestre","CategoriaGrupo","ImoCodigo","Moeda","ConverterMoeda","MoedaIndice","CodigoEmpresa","CodigoEmp","CodigoEmpreendimento","CodigoCategoria","CodigoMoeda","CodigoProprietario","Proprietario","FotoProprietario","DataHoraAtualizacao","VideoDestaque","VideoDestaqueTipo","FotoDestaque","FotoDestaquePequena","FotoDestaqueEmpreendimento","FotoDestaqueEmpreendimentoPequena","Latitude","Longitude","Agenciador","CodigoCorretor","CodigoAgencia","TotalBanheiros", "EmpreendimentoSite");
                
                /*
                    adicionados
                    'AceitaPermuta','AceitaPermutaCarro','AceitaPermutaOutro','AceitaPermutaTipoVeiculo','AlugarJaDestaque','AnoMinimoVeicPermuta','AreaBoxPrivativa','AreaBoxTotal','AreaConstruida','AreaTerreno','ClasseDoImovel','CodigoMalote','CorretorPrimeiroAge','DataDisponibilizacao','DataEntrega','DataFimAutorizacao','DataInicioAutorizacao','DataLancamento','DescricaoEmpreendimento','DestaqueWeb','EEmpreendimento','EmitiuNotaFiscalComissao','ExclusivoCorretor','ExibirComentarios','FolhaSPModelo','GrupoSPTipoOferta','HoraDomFim','HoraDomInicio','HoraFerFim','HoraFerInicio','HoraSabFim','HoraSabInicio','HoraSegSexFim','HoraSegSexInicio','ImovelaVendaDestaqueImovelaVenda','ImovelwebModelo','ImportadoMalote','Incompleto','Incorporadora','InformacaoVenda','InicioObra','InscricaoMunicipal','KeywordsWeb','Limpeza','LocacaoAnual','LocacaoTemporada','LocalizacaoPermuta','LojasEdificio','Lote','LugarCertoDestaqueLugarCerto','MercadoLivreTipoML','MesConstrucao','Midia','Modulos','Monitoramento','ObsLocacao','ObsVenda','PadraoConstrucao','Pavimentos','Plantao','PlantaoNoLocal','PorteEstrutural','Posicao','PosicaoAndar','PotenciaKVA','PrazoDesocupacao','ProjetoArquitetonico','Proposta','PropostaLocacao','QntDormitoriosPermuta','QntGaragensPermuta','QntSuitesPermuta','QTDGalpoes','QtdVarandas','Quadra','Reajuste','Regiao','ResponsavelReserva','Setor','SummerSale','TextoAnuncio','TipoImovel','TipoImovelPermuta','TipoOferta321Achei','TipoTeto','TiqueImoveisEmDestaque','TotalComissao','VagasCobertas','VagasDescobertas','ValorACombinar','ValorComissao','ValorCondominioM2','ValorDiaria','ValorIPTUM2','ValorLimpeza','ValorLivreProprietario','ValorLocacaoM2','ValorM2','ValorPermutaImovel','ValorTotalAluguel','ValorVendaM2','Venda','VigiaExterno','VigiaInterno','Visita','ZeladorNome','ZeladorTelefone'
                    
                    removidos
                    "Associada","Dacoes","DataEntradaRgi","DataSaidaRgi","DependenciadeEmpregada","EmDestaque","EstudaDacao","E_Empreendimento","GMapsLatitude","GMapsLongitude","ImobiliariaCadastro","ImovelEmCampanha","LinkTour","ObsAgenciou","Operacao","OrdenamentoEmpresa","OrientacaoSolar","PrazoEntrega","Reformado","ValorVendaDe"

                */

                $ambos = array_intersect($antigos, $novos);

                $fields_detalhe = array(
                    'Caracteristicas',
                    'InfraEstrutura',
                    array(
                        "Foto" => array("Ordem", "Codigo", "ImagemCodigo", "Data", "Descricao", "Destaque", "ExibirNoSite", "ExibirSite", "Foto", "Tipo")
                    ),
                    array(
                        "FotoEmpreendimento" => array("Foto", "Codigo", "Ordem", "Data", "Descricao", "Destaque", "Tipo", "ExibirNoSite")
                    ),
                    array(
                        "Anexo" => array("Codigo", "CodigoAnexo", "Descricao", "Anexo", "Arquivo", "ExibirNoSite", "ExibirSite")
                    ),
                    array(
                        "Video" => array("Codigo", "VideoCodigo", "Data", "Descricao", "DescricaoWeb", "Destaque", "ExibirNoSite", "ExibirSite", "Video", "Tipo")
                    )
                );

                $fields = array_merge($ambos, $fields_detalhe);

                $promise = static::detalhesAsync($codigo, $fields);

                /*
                $promise->then(function ($imovel) {
                    
                    static::saveImovel($imovel);
                })->otherwise(function($err) {
                    var_dump($err->getResponse()->json());
                    return $err;
                });
                */

                $promises[$codigo] = $promise;
            }

            $atualizados += count($atualizar);

            if (!empty($promises)) {
                Api::wait($promises, array(
                    'pool_size' => 10,
                    'complete' => function ($event) {
                        $response = $event->getResponse();
                        $imovel = json_decode($response->getBody()->getContents());
                        static::saveImovel($imovel);
                    },
                    'error' => function ($err) {
                        var_dump($err->getMessage());
                    }
                ));
            }
            $time = microtime(true) - $start;
            echo "\nasync time[{$pagina}/{$paginas}]: " . number_format($time, 5, ',', '.') . "\n";
            echo "async requests: " . count($promises) . "\n";

            $pagina = $paginacao->pagina + 1;
        } while ($pagina <= $paginas);

        foreach ($remover as $codigo => $imovel) {
            unset($indice[$codigo]);
            static::removeImovel($codigo);
        }
        static::setIndice($indice);

        $removidos = count($remover);

        if ($atualizados + $removidos != 0) {
            Tipos::update();
            Bairros::update();
            Cidades::update();
            Caracteristicas::update();
            Indices::update();
        }
    }

    public static function getAllImoveis()
    {
        if (null === static::$imoveis) {
            $indice = Imoveis::getIndice();

            static::$imoveis = array_map(function ($imovel) {
                return static::loadImovel($imovel->Codigo);
            }, $indice);
        }
        return static::$imoveis;
    }

    public static function formatImovel($imovel) {

        foreach ($imovel->Caracteristicas as $titulo => $possui) {
            if ($possui == 'Nao' || $possui == '') {
                continue;
            }
            if ($possui != 'Sim') {
                $key = preg_replace('#\s+#', '', $titulo);
                $imovel->{$key} = $possui;
            }
        }

        foreach ($imovel->InfraEstrutura as $titulo => $possui) {
            if ($possui == 'Nao' || $possui == '') {
                continue;
            }
            if ($possui != 'Sim') {
                $key = preg_replace('#\s+#', '', $titulo);
                $imovel->{$key} = $possui;
            }
        }
        
        $row = (object) array(
            'id' => (int) $imovel->ImoCodigo,
            'placa' => $imovel->Codigo,
            'empreendimento' => $imovel->Empreendimento,
            'codigo_empreendimento' => $imovel->CodigoEmpreendimento,
            'uf' => $imovel->UF,
            'cep' => $imovel->CEP,
            'cidade' => $imovel->Cidade,
            'logradouro' => $imovel->Endereco,
            'numero' => $imovel->Numero,
            'resumo' => $imovel->DescricaoWeb,
            'filial_id' => (int) $imovel->CodigoAgencia,
            'tipo_id' => (int) $imovel->CodigoCategoria,
            'indice_id' => (int) $imovel->CodigoMoeda,
            'lat' => (float) $imovel->Latitude,
            'lng' => (float) $imovel->Longitude,
            'area' => (float) $imovel->AreaPrivativa,
            'area_total' => (float) $imovel->AreaTotal,
            'valor' => (float) $imovel->ValorVenda,
            'valor_iptu' => (float) $imovel->ValorIptu,
            'valor_condominio' => (float) $imovel->ValorCondominio,
            'banheiros' => (int) $imovel->BanheiroSocialQtd,
            'suites' => (int)$imovel->Suites,
            'dormitorios' => (int) $imovel->Dormitorios,
            'salas' => (int) $imovel->Salas,
            'vagas' => (int) $imovel->Vagas,
            'bairro_comercial' => $imovel->BairroComercial,
            'link_tour' => isset($imovel->LinkTour)?$imovel->LinkTour:'',
            'bairro' => $imovel->Bairro,
            'caracteristicas' => array(),
            'imagens' => array(),
            'link_youtube' => $imovel->VideoDestaque,
            //'is_empreendimento' => $imovel->EEmpreendimento == 'Sim' || $imovel->Empreendimento != '',
            'is_empreendimento' => $imovel->EEmpreendimento == 'Sim' || $imovel->EmpreendimentoSite == 'Sim',
            'EmpreendimentoSite' => $imovel->EmpreendimentoSite.'',
            'is_lancamento' => $imovel->Lancamento == 'Sim'
        );

        foreach ($imovel->Caracteristicas as $titulo => $possui) {
            if ($possui == 'Nao' || $possui == '') {
                continue;
            }
            if ($possui != 'Sim') {
                $key = preg_replace('#\s+#','', $titulo);
                $imovel->{$key} = $possui;
            }
            $caracteristica_id = Str::slugify($titulo);
            $row->caracteristicas[] = $caracteristica_id;
        }

        foreach ($imovel->InfraEstrutura as $titulo => $possui) {
            if ($possui == 'Nao' || $possui == '') {
                continue;
            }
            if ($possui != 'Sim') {
                $key = preg_replace('#\s+#','', $titulo);
                $imovel->{$key} = $possui;
            }
            $caracteristica_id = Str::slugify($titulo);
            $row->caracteristicas[] = $caracteristica_id;
        }

        foreach ($imovel->Foto as $foto) {
            $id = $foto->ImagemCodigo * 10000000;
            $id += $foto->Destaque == 'Sim' ? 1000000 : 9000000;
            $id += ((int)$foto->Ordem) * 1000;
            $id += (int) $foto->ImagemCodigo;
            $imagem = (object) array(
                'id' => $id,
                'planta' => $foto->Tipo == 'Planta',
                'imovel_id' => (int) $foto->Codigo,
                'path' => $foto->Foto,
                'url' => $foto->Foto,
                'empreendimento' => false
            );
            $row->imagens[] = $imagem;
        }
        foreach ($imovel->FotoEmpreendimento as $foto) {
            $id = $foto->ImagemCodigo * 10000000;
            $id += $foto->Destaque == 'Sim' ? 1000000 : 9000000;
            $id += ((int)$foto->Ordem) * 1000;
            $id += (int) $foto->ImagemCodigo;
            $imagem = (object) array(
                'id' => $id,
                'planta' => $foto->Tipo == 'Planta',
                'imovel_id' => (int) $foto->Codigo,
                'path' => $foto->Foto,
                'url' => $foto->Foto,
                'empreendimento' => true
            );
            $row->imagens[] = $imagem;
        }

        foreach ((array)$imovel->Video as $video) {
            $id = "{$video->Codigo}.{$video->VideoCodigo}";
            $video = (object) array(
                'id' => $id,
                'youtube_id' => $video->Video,
                'data' => $video->Data,
                'destaque' => 'Sim' == $video->Destaque,
                'descricao' => $video->Descricao,
                'descriacao_web' => $video->DescricaoWeb,
                'exibir_no_site' => 'Sim' == $video->ExibirNoSite,
                'tipo' => $video->Tipo
            );
            $row->videos[] = $video;
        }

        if (preg_match('/^(http[s]?:\/\/)?(www\.)?(m\.)?((youtu|y2u)\.be\/|youtube\.com(\.br)?\/(watch|((embed|e|v)\/)))([\?&].*v=)?(?<id>[^\/\&\?\n]+).*/', $row->link_youtube, $m)) {
            $row->youtube_id = $m['id'];
        } else {
            $row->youtube_id = '';
        }

        if ($row->bairro_comercial != $row->bairro && $row->bairro_comercial != '') {
            $row->bairro = $row->bairro_comercial;
        }

        return $row;
    }

    public static function getAll()
    {
        if (null === static::$imoveis) {
            $indice = Imoveis::getIndice();
            static::$imoveis = array();
            static::$codigos = array();
            foreach($indice as $codigo => $imovel) {
                $imovel = static::loadImovel($imovel->Codigo);
                $imovel = static::formatImovel($imovel);

                static::$imoveis[$imovel->id] = $imovel;
                static::$codigos[strtolower($codigo)] = $imovel->id;
                if ($codigo == 'VR14590') {
                    //Script::log("Venda::Destaques() " . print_r($destaques, true));
                    //return static::$imoveis;
                }
            }
        }
        return static::$imoveis;
    }

    public static function getCodigos() {
        if (null === static::$codigos) {
            static::getAll();
        }
        return static::$codigos;
    }

    public static function getById($id) {
        $imoveis = static::getAll();
        return isset($imoveis[$id]) ? $imoveis[$id] : null;
    }

    public static function getByCodigo($codigo) {
        $codigos = static::getCodigos();
        $codigo = strtolower($codigo);
        if (!isset($codigos[$codigo])) {
            return null;
        }
        $id = $codigos[$codigo];
        return static::getById($id);
    }
}



