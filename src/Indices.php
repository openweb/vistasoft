<?php

namespace VistaSoft;

use Behat\Transliterator\Transliterator;

class Indices
{
    private static $records = null;

    public static function getFilename()
    {
        $data_folder = Api::getDataFolder();
        return $data_folder . '/indices.php';
    }

    public static function loadData()
    {
        $filename = static::getFilename();
        static::reset();
        if (file_exists($filename)) {
            static::$records = (array)require $filename;
        }
    }

    public static function saveData()
    {
        $filename = static::getFilename();
        $content = '<?php return ' . var_export((array) static::$records, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function update()
    {
        $imoveis = Imoveis::getAllImoveis();
        foreach ($imoveis as $codigo => $imovel) {
            if ($imovel->ConverterMoeda == 'Sim') {                
                static::addImovel($imovel);
            }
        }
        static::saveData();
    }

    public static function reset()
    {
        static::$records = array();
    }

    public static function addImovel($imovel)
    {
        $id = $imovel->CodigoMoeda;
        $titulo = $imovel->Moeda;
        $indice = $imovel->MoedaIndice;

        static::$records[$id] = (object) array(
            'id' => $id,
            'titulo' => $titulo,
            'indice' => (double)$indice
        );
    }



    public static function getAll()
    {
        if (null === static::$records) {
            static::loadData();
        }
        return static::$records;
    }

    public static function getById($id)
    {
        $records = static::getAll();
        return isset($records[$id]) ? $records[$id] : null;
    }
}
