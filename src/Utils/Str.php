<?php

namespace VistaSoft\Utils;

use VRia\Utils\NoDiacritic;

class Str
{
    public static function slugify($str)
    {
        $str = strtolower(NoDiacritic::filter($str));
        $str = preg_replace('/[^\d\w]+/', ' ', $str);
        $str = preg_replace('/\s+/', '-', trim($str));
        return $str;
    }

    public static function titleify($str)
    {
        return mb_convert_case(mb_strtolower($str, 'utf-8'), MB_CASE_TITLE, 'utf-8');
    }
}
