<?php

namespace VistaSoft;

use VistaSoft\Utils\Str;

class Imagens
{
    public static function getByImovelId($imovel_id) {
        $imovel = Imoveis::getById($imovel_id);
        if (!isset($imovel->placa)) {
            $imovel = Imoveis::formatImovel($imovel);
        }
        return $imovel->imagens;
    }

    public static function getByImovelCodigo($codigo) {
        $imovel = Imoveis::getByCodigo($codigo);
        if (!isset($imovel->placa)) {
            $imovel = Imoveis::formatImovel($imovel);
        }
        return $imovel->imagens;
    }
}
