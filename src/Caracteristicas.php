<?php

namespace VistaSoft;

use VistaSoft\Utils\Str;

class Caracteristicas {
    private static $records = null;

    public static function getFilename()
    {
        $data_folder = Api::getDataFolder();
        return $data_folder . '/caracteristicas.php';
    }

    public static function loadData()
    {
        $filename = static::getFilename();
        static::reset();
        if (file_exists($filename)) {
            static::$records = (array)require $filename;
        }
    }

    public static function saveData()
    {
        $filename = static::getFilename();
        $content = '<?php return ' . var_export((array) static::$records, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function update()
    {
        $imoveis = Imoveis::getAllImoveis();
        foreach ($imoveis as $codigo => $imovel) {
            static::addImovel($imovel);
        }
        static::saveData();
    }

    public static function reset() {
        static::$records = array();
    }

    public static function addImovel($imovel) {
        foreach($imovel->Caracteristicas as $caracteristica => $possui) {
            if ($possui == 'Sim') {
                static::addCaracteristica($caracteristica);
            }
        }
        foreach ($imovel->InfraEstrutura as $caracteristica => $possui) {
            if ($possui == 'Sim') {
                static::addCaracteristica($caracteristica, true);
            }
        }
    }

    public static function addCaracteristica($titulo, $condominio = false) {
        $id = Str::slugify($titulo);
        $slug = $id;
        if (isset(static::$records[$id])) {
            return;
        }
        $row = (object)array(
            'id' => $id,
            'slug' => $slug,
            'titulo' => $titulo,
            'condominio' => $condominio,
            'imovel' => !$condominio
        );
        static::$records[$id] = $row;
    }

    public static function getAll() {
        if (null === static::$records) {
            static::loadData();
        }
        return static::$records;
    }

    public static function getBySlug($slug)
    {
        return static::getById($slug);
    }

    public static function getById($id)
    {
        $records = static::getAll();
        return isset($records[$id]) ? $records[$id] : null;
    }
}