<?php

namespace VistaSoft;

use GuzzleHttp\Client;
use GuzzleHttp\Pool;

class Api {
    private static $host;
    private static $key;
    private static $data_folder = null;
    private static $client = null;
    private static $last_update = null;

    public static function setup($host, $key, $data_folder = null) {
        static::setHost($host);
        static::setKey($key);
        static::setDataFolder($data_folder);
    }

    public static function setDataFolder($data_folder) {
        
        static::$data_folder = $data_folder;
    }

    public static function getDataFolder() {
        $data_folder = static::$data_folder;
        if (null === $data_folder) {
            $data_folder = sys_get_temp_dir() . '/VistaSoftCache';
        }
        if (!file_exists($data_folder)) {
            mkdir($data_folder, 0777, true);
        }
        return $data_folder;
    }

    public static function setHost($host) {
        static::$host = $host;
    }

    public static function getHost() {
        return static::$host;
    }

    public static function setKey($key) {
        static::$key = $key;
    }

    public static function getKey() {
        return static::$key;
    }

    public static function getClient() {
        if (null === static::$client) {
            $host   = static::getHost();            
            $client = new Client(array(
                'base_url'  => $host,
                'defaults'  => array(
                    'headers'   => array(
                        'Accept'    => 'application/json'
                        )
                )
            ));
            static::$client = $client;
        }
        return static::$client;
    }

    public static function exec($path, $query = array()) {
        $client = static::getClient();
        $query['key'] = static::getKey();
        $response = $client->get($path, array(
            'query' => $query
        ));

        return json_decode($response->getBody()->getContents());
    }

    public static function execAsync($path, $query = array()) {
        $client = static::getClient();
        $query['key'] = static::getKey();
        $request = $client->createRequest('GET',$path, array(
            'query' => $query,
            'future'=> 'lazy'
        ));

        return $request;
    }

    public static function getLastUpdateTime() {
        if (null !== static::$last_update) {
            return static::$last_update;
        }
        $data_folder = static::getDataFolder();
        $filename = $data_folder . '/last-update.txt';
        if (file_exists($filename)) {
            $last_update = file_get_contents($filename);
            static::$last_update = $last_update;
            return $last_update;
        }
        return 0;
    }

    public static function setLastUpdateTime($time) {
        $data_folder = static::getDataFolder();
        $filename = $data_folder . '/last-update.txt';
        static::$last_update = $time;
        file_put_contents($filename, $time);
    }

    public static function wait($requests, $options = array()) {
        $client = static::getClient();
        $pool = new Pool($client, $requests, $options);
        $pool->wait();
    }
}