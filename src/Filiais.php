<?php

namespace VistaSoft;

class Filiais {
    public static function listar($fields = array('Codigo','Nome','Bairro','Celular','Cep','Cidade','Cnpj','Codigo','Empresa','Endereco','Fone','Fone2','Pais','RazaoSocial','Uf','Site'))
    {
        $pesquisa = array(
            'fields' => $fields
        );
        $query = array(
            'pesquisa' => json_encode($pesquisa)
        );
        $filiais = Api::exec("/agencias/listar", $query);
        return (array)$filiais;
    }

    private static $filiais = null;

    public static function getFilename()
    {
        $data_folder = Api::getDataFolder();
        return $data_folder . '/filiais.php';
    }

    public static function loadData()
    {
        $filename = static::getFilename();
        static::reset();
        if (file_exists($filename)) {
            static::$filiais = require $filename;
        }
    }

    public static function saveData()
    {
        $filename = static::getFilename();
        $content = '<?php return (object)' . var_export((array) static::$filiais, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function update()
    {
        static::$filiais = static::listar();
        static::saveData();
    }

    public static function reset()
    {
        static::$filiais = array();
    }

    public static $records = null;

    public static function generate() {
        static::loadData();
        if (empty(static::$filiais)) {
            static::update();
        }
        $filiais = static::$filiais;
        return array_map(function($filial) {
            return (object)array(
                'id' => $filial->Codigo,
                'titulo' => $filial->Nome,
                'email' => ''
            );
        }, (array)$filiais);
    }

    public static function getAll()
    {
        if (null === static::$records) {
            static::$records = static::generate();
        }
        return static::$records;
    }

    public static function getById($id)
    {
        $records = static::getAll();
        return isset($records[$id]) ? $records[$id] : null;
    }
}