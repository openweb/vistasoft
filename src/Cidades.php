<?php

namespace VistaSoft;

use VistaSoft\Utils\Str;

class Cidades {
    private static $records = null;

    public static function getFilename()
    {
        $data_folder = Api::getDataFolder();
        return $data_folder . '/cidades.php';
    }

    public static function loadData()
    {
        $filename = static::getFilename();
        static::reset();
        if (file_exists($filename)) {
            static::$records = (array) require $filename;
        }
    }

    public static function saveData()
    {
        $filename = static::getFilename();
        $content = '<?php return ' . var_export((array) static::$records, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function update()
    {
        $bairros = Bairros::getAll();
        foreach ($bairros as $codigo => $bairro) {
            static::addBairro($bairro);
        }
        static::saveData();
    }

    public static function reset() {
        static::$records = array();
    }

    public static function addBairro($bairro) {
        $titulo = $bairro->cidade;
        $uf = $bairro->uf;
        $id = $bairro->cidade_id;

        static::$records[$id] = (object)array(
            'id' => $id,
            'uf' => $uf,
            'titulo' => $titulo
        );

    }


    public static function getAll()
    {
        if (null === static::$records) {
            static::loadData();
        }
        return static::$records;
    }

    public static function getById($id)
    {
        $cidade_id = Str::slugify($id);
        $records = static::getAll();
        return isset($records[$cidade_id]) ? $records[$cidade_id] : null;
    }
}