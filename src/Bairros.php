<?php

namespace VistaSoft;

use VistaSoft\Utils\Str;

class Bairros {
    private static $records = null;

    public static function getFilename()
    {
        $data_folder = Api::getDataFolder();
        return $data_folder . '/bairros.php';
    }

    public static function loadData()
    {
        $filename = static::getFilename();
        static::reset();
        if (file_exists($filename)) {
            static::$records = require $filename;
        }
    }

    public static function saveData()
    {
        $filename = static::getFilename();
        $content = '<?php return (object)' . var_export((array) static::$records, true) . ';';

        file_put_contents($filename, str_replace('stdClass::__set_state', '(object)', $content));
    }

    public static function update()
    {
        $imoveis = Imoveis::getAllImoveis();
        foreach ($imoveis as $codigo => $imovel) {
            if ($imovel->Latitude != 0 && $imovel->Longitude != 0) {
                static::addImovel($imovel);
            }
        }
        static::saveData();
    }

    public static function reset() {
        static::$records = (object)array(
            'records' => array(),
            'cidades' => array()
        );
    }

    public static function addImovel($imovel) {
        $uf = $imovel->UF;
        $cidade = Str::titleify($imovel->Cidade);
        $bairro = Str::titleify($imovel->Bairro);
        $bairro_comercial = Str::titleify($imovel->BairroComercial);

        $cidade_slug = Str::slugify($cidade . " " . $uf);
        $bairro_slug = Str::slugify($bairro . ' ' . $cidade . ' ' . $uf);
        $bairro_comercial_slug = Str::slugify($bairro_comercial . ' ' . $cidade . ' ' . $uf);

        $row = (object) array(
            'id' => $bairro_slug,
            'cidade_id' => $cidade_slug,
            'bairro_id' => $bairro_slug,
            'uf' => $uf,
            'cidade' => $cidade,
            'titulo' => $bairro
        );

        static::$records->records[$bairro_slug] = $row;
        if (!isset(static::$records->cidades[$cidade_slug])) {
            static::$records->cidades[$cidade_slug] = array();
        }
        static::$records->cidades[$cidade_slug][$bairro_slug] = $row;

        Cidades::addBairro($row);

        if ($bairro_comercial_slug != $bairro_slug) {
            $row = (object) array(
                'id' => $bairro_comercial_slug,
                'cidade_id' => $cidade_slug,
                'bairro_id' => $bairro_comercial_slug,
                'uf' => $uf,
                'cidade' => $cidade,
                'titulo' => $bairro_comercial
            );

            static::$records->records[$bairro_comercial_slug] = $row;
            if (!isset(static::$records->cidades[$cidade_slug])) {
                static::$records->cidades[$cidade_slug] = array();
            }
            static::$records->cidades[$cidade_slug][$bairro_comercial_slug] = $row;            
        }
    }

    public static function getData()
    {
        if (null === static::$records) {
            static::loadData();
        }
        return static::$records;
    }

    public static function getAll()
    {
        $records = (object)static::getData();
        return $records->records;
    }

    public static function getCidades()
    {
        $records = (object)static::getData();
        return $records->cidades;
    }

    public static function getById($id)
    {
        $records = static::getAll();
        return isset($records[$id]) ? $records[$id] : null;
    }

    public static function getByCidade($cidade)
    {
        $cidade_id = Str::slugify($cidade);
        $records = static::getCidades();
        $records = isset($records[$cidade_id]) ? $records[$cidade_id] : array();
        return array_values($records);
    }

    public static function getByBairro($cidade, $bairro)
    {
        $cidade_id = Str::slugify($cidade);
        $bairro_id = Str::slugify($bairro);
        $records = static::getCidades();
        $records = isset($records[$cidade_id]) ? $records[$cidade_id] : array();
        return isset($records[$bairro_id]) ? $records[$bairro_id] : null;
    }
}
