<?php

use Behat\Transliterator\Transliterator;
use VistaSoft\Api;
use VistaSoft\Bairros;
use VistaSoft\Caracteristicas;
use VistaSoft\Cidades;
use VistaSoft\Filiais;
use VistaSoft\Imoveis;
use VistaSoft\Indices;
use VistaSoft\Tipos;

require 'vendor/autoload.php';

Api::setup('http://vilarica-rest.vistahost.com.br/', 'e8e784a96daf40964790992985f96078', __DIR__.'/data/cache');
/*
$fields = array("123iPublicar123i","321AcheiPublicar321Achei","AceitaDacao","AceitaFinanciamento","AdministradoraCondominio","AnoConstrucao","AptosAndar","AptosEdificio","AreaPrivativa","AreaTotal","Associada","Bairro","BairroComercial","BanheiroSocialQtd","Bloco","CampanhaImportacao","Categoria","CEP","Chave","ChaveFacilPublicarChaveFacil","ChaveNaAgencia","ChavesNaMaoDestaque","Cidade","ClicMeuImovelPublicarClicMeuImovel","Complemento","Construtora","Dacoes","DataAtualizacao","DataCadastro","DataEntradaRgi","DataHoraAtualizacao","DataImportacao","DataSaidaRgi","DependenciadeEmpregada","DescricaoWeb","DimensoesTerreno","Dormitorios","EmDestaque","EmpOrulo","Empreendimento","Endereco","EstudaDacao","Exclusivo","ExibirNoSite","E_Empreendimento","Finalidade","Garagem","GaragemNumeroBox","GaragemTipo","GMapsLatitude","GMapsLongitude","Imediacoes","ImobiliariaCadastro","ImoboxPublicarImobox","ImovelaVendaPublicarImovelaVenda","ImovelClassPublicarImovelClass","ImovelEmCampanha","ImovelwebPublicarImovelweb","Lancamento","LinkLarPublicarLinkLar","LinkTour","Matricula","MinhaPrimeiraCasaPublicarMinhaPrimeiraCasa","ModeloImovelWeb","Numero","ObsAgenciou","Observacoes","ObsOrulo","Ocupacao","OLXFinalidadesPublicadas","OlxPublicarOlx","Operacao","OrdenamentoEmpresa","Orientacao","OrientacaoSolar","Orulo","Pais","PercentualComissao","PortoAlegreImoveisPublicarPortoAlegreImoveis","PrazoEntrega","Prestacao","ProperatiPublicarProperati","PublicarChavesNaMao","RedimobPublicarRedimob","Referencia","ReferenciaAntigaGaia","ReferenciaGaia","Reformado","SaldoDivida","Situacao","Status","StatusMigrado","StoriaImoveisPublicarStoriaImoveis","Suites","SuperDestaqueWeb","TaTriPublicarTaTri","TemPlaca","TipoEndereco","TituloSite","TodaOfertaPublicarTodaOferta","UF","URLVideo","Vagas","ValorCondominio","ValorIptu","ValorLocacao","ValorVenda","ValorVendaDe","VisitaAcompanhada","VivaRealDestaqueVivaReal","VivarealPublicarVivareal","WebCasasPublicarWebCasas","WebEscrit\u00f3riosDestaque","WebEscritoriosDestaque","ZapPublicarZap","ZapTipoOferta","Zona","CategoriaImovel","CategoriaMestre","CategoriaGrupo","ImoCodigo","Moeda","ConverterMoeda","MoedaIndice","CodigoEmpresa","CodigoEmp","CodigoEmpreendimento","CodigoCategoria","CodigoMoeda","CodigoProprietario","Proprietario","FotoProprietario","VideoDestaque","VideoDestaqueTipo","FotoDestaque","FotoDestaquePequena","FotoDestaqueEmpreendimento","FotoDestaqueEmpreendimentoPequena","Latitude","Longitude","Agenciador","CodigoCorretor","CodigoAgencia","TotalBanheiros");
$fields_novos = array(
    "AceitaDacao",
    "AceitaFinanciamento",
    "AceitaPermuta",
    "AceitaPermutaCarro",
    "AceitaPermutaOutro",
    "AceitaPermutaTipoVeiculo",
    "AdministradoraCondominio",
    "AlugarJaDestaque",
    "AnoConstrucao",
    "AnoMinimoVeicPermuta",
    "AptosAndar",
    "AptosEdificio",
    "AreaBoxPrivativa",
    "AreaBoxTotal",
    "AreaConstruida",
    "AreaPrivativa",
    "AreaTerreno",
    "AreaTotal",
    "Bairro",
    "BairroComercial",
    "BanheiroSocialQtd",
    "Bloco",
    "CampanhaImportacao",
    "Categoria",
    "CEP",
    "Chave",
    "ChaveNaAgencia",
    "ChavesNaMaoDestaque",
    "Cidade",
    "ClasseDoImovel",
    "CodigoMalote",
    "Complemento",
    "Construtora",
    "CorretorPrimeiroAge",
    "DataAtualizacao",
    "DataCadastro",
    "DataDisponibilizacao",
    "DataEntrega",
    "DataFimAutorizacao",
    "DataImportacao",
    "DataInicioAutorizacao",
    "DataLancamento",
    "DescricaoEmpreendimento",
    "DescricaoWeb",
    "DestaqueWeb",
    "DimensoesTerreno",
    "Dormitorios",
    "EEmpreendimento",
    "EmitiuNotaFiscalComissao",
    "EmpOrulo",
    "Empreendimento",
    "Endereco",
    "Exclusivo",
    "ExclusivoCorretor",
    "ExibirComentarios",
    "ExibirNoSite",
    "Finalidade",
    "FolhaSPModelo",
    "GaragemNumeroBox",
    "GaragemTipo",
    "GrupoSPTipoOferta",
    "HoraDomFim",
    "HoraDomInicio",
    "HoraFerFim",
    "HoraFerInicio",
    "HoraSabFim",
    "HoraSabInicio",
    "HoraSegSexFim",
    "HoraSegSexInicio",
    "Imediacoes",
    "ImovelaVendaDestaqueImovelaVenda",
    "ImovelwebModelo",
    "ImportadoMalote",
    "Incompleto",
    "Incorporadora",
    "InformacaoVenda",
    "InicioObra",
    "InscricaoMunicipal",
    "KeywordsWeb",
    "Lancamento",
    "Limpeza",
    "LocacaoAnual",
    "LocacaoTemporada",
    "LocalizacaoPermuta",
    "LojasEdificio",
    "Lote",
    "LugarCertoDestaqueLugarCerto",
    "Matricula",
    "MercadoLivreTipoML",
    "MesConstrucao",
    "Midia",
    "Modulos",
    "Monitoramento",
    "Numero",
    "Observacoes",
    "ObsLocacao",
    "ObsVenda",
    "Ocupacao",
    "OLXFinalidadesPublicadas",
    "Orientacao",
    "Orulo",
    "PadraoConstrucao",
    "Pais",
    "Pavimentos",
    "PercentualComissao",
    "Plantao",
    "PlantaoNoLocal",
    "PorteEstrutural",
    "Posicao",
    "PosicaoAndar",
    "PotenciaKVA",
    "PrazoDesocupacao",
    "Prestacao",
    "ProjetoArquitetonico",
    "Proposta",
    "PropostaLocacao",
    "QntDormitoriosPermuta",
    "QntGaragensPermuta",
    "QntSuitesPermuta",
    "QTDGalpoes",
    "QtdVarandas",
    "Quadra",
    "Reajuste",
    "Referencia",
    "Regiao",
    "ResponsavelReserva",
    "SaldoDivida",
    "Setor",
    "Situacao",
    "Status",
    "Suites",
    "SummerSale",
    "SuperDestaqueWeb",
    "TemPlaca",
    "TextoAnuncio",
    "TipoEndereco",
    "TipoImovel",
    "TipoImovelPermuta",
    "TipoOferta321Achei",
    "TipoTeto",
    "TiqueImoveisEmDestaque",
    "TituloSite",
    "TotalComissao",
    "UF",
    "URLVideo",
    "Vagas",
    "VagasCobertas",
    "VagasDescobertas",
    "ValorACombinar",
    "ValorComissao",
    "ValorCondominio",
    "ValorCondominioM2",
    "ValorDiaria",
    "ValorIptu",
    "ValorIPTUM2",
    "ValorLimpeza",
    "ValorLivreProprietario",
    "ValorLocacao",
    "ValorLocacaoM2",
    "ValorM2",
    "ValorPermutaImovel",
    "ValorTotalAluguel",
    "ValorVenda",
    "ValorVendaM2",
    "Venda",
    "VigiaExterno",
    "VigiaInterno",
    "Visita",
    "VisitaAcompanhada",
    "VivaRealDestaqueVivaReal",
    "WebEscritoriosDestaque",
    "ZapTipoOferta",
    "ZeladorNome",
    "ZeladorTelefone",
    "Zona",
    "CategoriaImovel",
    "CategoriaMestre",
    "CategoriaGrupo",
    "ImoCodigo",
    "Moeda",
    "ConverterMoeda",
    "MoedaIndice",
    "CodigoEmpresa",
    "CodigoEmp",
    "CodigoEmpreendimento",
    "CodigoCategoria",
    "CodigoMoeda",
    "CodigoProprietario",
    "Proprietario",
    "FotoProprietario",
    "DataHoraAtualizacao",
    "VideoDestaque",
    "VideoDestaqueTipo",
    "FotoDestaque",
    "FotoDestaquePequena",
    "FotoDestaqueEmpreendimento",
    "FotoDestaqueEmpreendimentoPequena",
    "Latitude",
    "Longitude",
    "Agenciador",
    "CodigoCorretor",
    "CodigoAgencia",
    "TotalBanheiros");
$disponiveis = array();
$novos = array();
$removidos = array();
foreach($fields as $field) {
    try {
        Api::exec("/imoveis/listar", array(
            "pesquisa" => json_encode(array(
                'fields' => array("Codigo", $field)
            ))
        ));
        $disponiveis[] = $field;
    } catch (\Exception $err) {
    }
}

foreach($disponiveis as $field) {
    if (!in_array($field,$fields_novos)) {
        $removidos[] = $field;
    }
}

echo "\n".'"'.join('","',$removidos).'"'."\n";

exit;
*/
//Imoveis::update();
//Filiais::update();
Indices::update();
exit;



//print_r($imoveis);

$start = microtime(true);
$imoveis = Imoveis::getAllImoveis();
echo "\nend time: " . (microtime(true) - $start) . "s\n";

$start = microtime(true);
$tipos = Tipos::getAll();
echo "\nend time: " . (microtime(true) - $start) . "s\n";

$start = microtime(true);
$bairros = Bairros::getAll();
echo "\nend time: " . (microtime(true) - $start) . "s\n";


$start = microtime(true);
$cidades = Cidades::getAll();
echo "\nend time: " . (microtime(true) - $start) . "s\n";


$start = microtime(true);
$caracteristicas = Caracteristicas::getAll();
echo "\nend time: " . (microtime(true) - $start) . "s\n";

//var_dump($caracteristicas);

$indice = Imoveis::getIndice();
$fh = fopen(__DIR__.'/data/indice.csv', 'w');
$fh2 = fopen(__DIR__.'/data/imovel_caracteristicas.csv', 'w');

foreach($imoveis as $codigo => $row) {
    fputcsv($fh, array($codigo, (int)$row->CodigoCategoria, $row->Categoria, (double)$row->Latitude, (double)$row->Longitude, $row->UF, $row->Cidade, $row->Bairro, (double)$row->ValorVenda, (int)$row->Dormitorios, (int)$row->Vagas));

    foreach($row->Caracteristicas as $titulo => $possui) {
        if ($possui == 'Sim') {
            fputcsv($fh2, array($codigo, $titulo, 0));
        }
    }

    foreach ($row->InfraEstrutura as $titulo => $possui) {
        if ($possui == 'Sim') {
            fputcsv($fh2, array($codigo, $titulo, 0));
        }
    }
}

fclose($fh);
fclose($fh2);

$content = gzcompress(file_get_contents(__DIR__ . '/data/indice.csv'), 9);
file_put_contents(__DIR__ . '/data/indice.tar.gz', $content);

$content = gzcompress(file_get_contents(__DIR__ . '/data/imovel_caracteristicas.csv'), 9);
file_put_contents(__DIR__ . '/data/imovel_caracteristicas.tar.gz', $content);